#include "smartcard.h"
#include "port.h"
//#include "UART.h"
//#include "main.h"
//#include "port.h"
//#include  <xc.h>
//#include "switch.h"
//#include"OS.h"
//#include "isr.h"
//#include"pwm.h"
//#include "timer.h"
#include "r_cg_wdt.h"
#include "r_cg_macrodriver.h"
#include "r_cg_sau.h"
//#include "r_cg_pclbuz.h"
#include "r_cg_tau.h"
#include "switch.h"
#include "wrp_mcu.h"                /* MCU Wrapper Layer definitions */
/* Exported constants --------------------------------------------------------*/
#define T0_PROTOCOL               0x00  /* T0 protocol */
#define T1_PROTOCOL               0x01  /* T1 protocol */
#define DIRECT                    0x3B  /* Direct bit convention */
#define INDIRECT                  0x3F  /* Indirect bit convention */
#define MAX_PROTOCOL_LEVEL         7  /* Maximun levels of protocol */
#define MAX_INTERFACE_BYTE         4  /* Maximum number of interface bytes per protocol */
#define HIST_LENGTH               20

/*-------------------------- Smartcard Interface Byte-------------------------*/
#define SC_INTERFACE_BYTE_TA    0 /* Interface byte TA(i) */
#define SC_INTERFACE_BYTE_TB    1 /* Interface byte TB(i) */
#define SC_INTERFACE_BYTE_TC    2 /* Interface byte TC(i) */
#define SC_INTERFACE_BYTE_TD    3 /* Interface byte TD(i) */

/*********** Prototype *******************/

typedef struct
{
    uint8_t Status;     /* The Presence of the Interface byte*/
    uint8_t Value;      /* The Value of the Interface byte*/
} tSC_InterfaceByte;
// Protocol Level structure - ------------------------------------------------
typedef struct
{
    tSC_InterfaceByte InterfaceByte[MAX_INTERFACE_BYTE];      /* The Values of the Interface byte TA(i), TB(i), TC(i)and TD(i)*/
} tSC_ProtocolLevel;
/* ATR structure - Answer To Reset -------------------------------------------*/
typedef struct
{
  uint8_t TS;                                 /* Bit Convention Direct/Indirect */
  uint8_t T0;                                 /* Each bit in the high nibble = Presence of the further interface byte;
                //                                 Low nibble = Number of historical byte */
  tSC_ProtocolLevel T[MAX_PROTOCOL_LEVEL];      /* Setup array */
  uint8_t H[HIST_LENGTH];                     /* Historical array */
  uint8_t Tlength;                            /* Setup array dimension */
  uint8_t Hlength;                            /* Historical array dimension */
  uint8_t TCK;                                /* Check character */
  uint8_t CLK_Mod_TA1;                       /* F & D modification values*/
  uint8_t Vpp_TB1;                           /* programming voltage */
  uint8_t GT_TC1;                               /* extra guard Time bet bytes */
  uint8_t T1Prot_LSB_TD1;                           /* If protocol T1 is used  */
  } tSC_ATR;
tSC_ATR SC_A2R;

//static  uint8_t ATRReady=0; 
static tSC_State SCState;
static tSC_State PrevSCState;
static uint8_t SC_ATR_Size = 40;

static uint8_t CharErrorFlag = 0;
static uint8_t UartRxData[100];


static tSC_ADPU_Commands SC_ADPU_Command;
static tSC_ADPU_Response SC_APDU_Response;




static void A2R_Clear (void); //Reset all A2R sturcture
static uint8_t SC_GetATR(void);
static uint8_t DecodeATR(uint8_t *card);  /* Decode ATR / return T0 or T1 protocol */
static void ProcessData(void);// perform any process or calculation from ATR received data
static void Activation(void);             //send activation sequence 
static void Deactivation(void);       //  deactivation sequence
static void WaitRxData(void);
/************************************/
void ReadResponseData(uint8_t * data);// receive the data and response from sc

/*------------------------  used command arrays -------------------------------*/

void SC_Init(void)
{

	SC_RESET_PIN =1;  // puts reset signal to high state
	R_UART0_Receive(UartRxData,100);
    SCState = SC_IDLE;
   // P3_bit.no1 =1; //test pin
}



static void Activation(void) // perform activation sequence
{  
	R_TAU0_Channel5_Start();// clk
 	R_UART0_Start();
 	SC_RESET_PIN =0;  // RST signal low
	R_TAU0_Channel4_Start(); // RST signal high after 200 us

}

static void Deactivation(void)// perform deactivation sequence
{
    
    A2R_Clear();   // clear ATR structure
    SC_RESET_PIN =1;  // puts reset signal to high state
   R_TAU0_Channel5_Stop();// stop clock
    R_UART0_Stop();// stop uart module
    SCState = SC_IDLE;// return sc state to ideal state
}


static void WaitRxData(void)       // wait data received from sc (after ATR of after APDU commands)
{
    static uint16_t SC_WaitCounter=0;
   
    SC_WaitCounter+=SC_PERIOD_MS; // my tick SC_PERIOD_MS 
    if ((SC_WaitCounter!=(SC_WAIT_PERIOD_MS)))// wait period for SC data communication should be <= WT 
    {
        return;
    }

    switch(PrevSCState)
     {
        case SC_ACTIVATION: 
                  // clear , decode , process ATR data
            
            if (SC_GetATR()== 1) // the start of ATR data is valid , then change state
            {
                SCState = SC_APDU;// change state to APDU state if ATR is received correctly

            }
            else 
            {
                SCState = SC_DEACTIVATE; // deactivate in case of not valid ATR
            }    

         
            break;
        case SC_APDU:
            // here we should perform data exchange ( read ) operation according to card type and card function 
           ReadResponseData(UartRxData);
            SCState = SC_DEACTIVATE;   // after data session , we should deactivate
            break; 

        default:
            break;
    }

    SC_WaitCounter=0;
    R_UART0_Receive(UartRxData,100);// reset uart data index to initial position in UartRxData array
return;
 }
  

/*************************************  Process ART  *******************************/
static void ProcessData(void)  // here we make some processes (not important )
{ 
    SC_A2R.CLK_Mod_TA1   = SC_A2R.T[0].InterfaceByte[SC_INTERFACE_BYTE_TA].Value;
    SC_A2R.Vpp_TB1       = SC_A2R.T[0].InterfaceByte[SC_INTERFACE_BYTE_TB].Value;
    SC_A2R.GT_TC1        = SC_A2R.T[0].InterfaceByte[SC_INTERFACE_BYTE_TC].Value;
    SC_A2R.T1Prot_LSB_TD1= SC_A2R.T[0].InterfaceByte[SC_INTERFACE_BYTE_TD].Value;
}
/*************************************  Decode ART  *******************************/
static uint8_t SC_GetATR(void)// check ATR data
{  
     A2R_Clear();
     DecodeATR(UartRxData);
     ProcessData();
     if ((SC_A2R.T0 !=0) &&( SC_A2R.TS == 0x3b))   // validate ATR data
         return 1;
     else 
         return 0;
 }
/*************************************  Reset all A2R sturcture *******************************/

static void  A2R_Clear (void)
{
    

    uint16_t i, j  = 0;

        /* Reset SC_A2R Structure --------------------------------------------*/
        SC_A2R.TS = 0;
        SC_A2R.T0 = 0;
        for (i = 0; i < MAX_PROTOCOL_LEVEL; i++)
        {
            for (j = 0; j < MAX_INTERFACE_BYTE; j++)
            {
              SC_A2R.T[i].InterfaceByte[j].Status = 0;
              SC_A2R.T[i].InterfaceByte[j].Value = 0;
            }
        }
        for (i = 0; i < HIST_LENGTH; i++)
        {
          SC_A2R.H[i] = 0;
        }

        SC_A2R.Tlength = 0;
        SC_A2R.Hlength = 0;
 
}

 /**
  * @brief  Decodes the Answer to reset received from card.
  * @param  card: pointer to the buffer containing the card ATR.
  * @retval None
  */
static uint8_t DecodeATR(uint8_t *card)
{
  uint16_t i = 0, flag = 0, protocol = 0;
  uint8_t index = 0, level = 0;

    for (index = 0;index<=SC_ATR_Size; index++)     //searching for the first ATR valid index byte
    {
        if (((card[index] == 0x3b)||(card[index] == 0x3f))&&(card[index+1] != 0x00))
        {            
            break;
        } 

    }
/******************************TS/T0 Decode************************************/
  SC_A2R.TS = card[index];  /* Initial character */
  index = index+1;
  SC_A2R.T0 = card[index];  /* Format character */
  index = index+1;
/*************************Historical Table Length Decode***********************/
  SC_A2R.Hlength = SC_A2R.T0 & (uint8_t)0x0F;

/******************************Protocol Level(1) Decode************************/
  /* Check TD(1) if present */
  if ((SC_A2R.T0 & (uint8_t)0x80) == 0x80)
  {
    flag = 1;       // flag == 1 then TD(1) exist 
  }

  /* Each bits in the T0 high nibble(b8 to b5) equal to 1 indicates the presence
     of a further interface byte */
  for (i = 0; i < 4; i++)
  {
    if (((SC_A2R.T0 & (uint8_t)0xF0) >> (4 + i)) & (uint8_t)0x1)
    {
      SC_A2R.T[level].InterfaceByte[i].Status = 1;
      SC_A2R.T[level].InterfaceByte[i].Value = card[index];
      index = index+1;
      SC_A2R.Tlength++;
    }
  }
  
  
/*****************************T Decode*****************************************/
  if (SC_A2R.T[level].InterfaceByte[3].Status == 1)    // interface byte 3 is TD
  {
    /* Only the protocol(parameter T) present in TD(1) is detected
       if two or more values of parameter T are present in TD(1), TD(2)..., so the
       firmware should be updated to support them */
    protocol = SC_A2R.T[level].InterfaceByte[SC_INTERFACE_BYTE_TD].Value  & (uint8_t)0x0F;
  }
  else
  {
    protocol = 0;
  }

  /* Protocol Level Increment */

/******************************Protocol Level(n>1) Decode**********************/
  while (flag)
  {
    if ((SC_A2R.T[level].InterfaceByte[SC_INTERFACE_BYTE_TD].Value & (uint8_t)0x80) == 0x80)
    {
      flag = 1;
    }
    else
    {
      flag = 0;
    }
    /* Each bits in the high nibble(b8 to b5) for the TD(i) equal to 1 indicates
      the presence of a further interface byte */
    for (i = 0; i < 4; i++)
    {
      if (((SC_A2R.T[level].InterfaceByte[SC_INTERFACE_BYTE_TD].Value & (uint8_t)0xF0) >> (4 + i)) & (uint8_t)0x1)
      {
        SC_A2R.T[level+1].InterfaceByte[i].Status = 1;
        SC_A2R.T[level+1].InterfaceByte[i].Value = card[index];

        SC_A2R.Tlength++;
      }
    }
    level++;
  }

  for (i = 0; i < SC_A2R.Hlength; i++)
   {
     SC_A2R.H[i] = card[i + 2 + SC_A2R.Tlength];
   }
/*************************************TCK Decode*******************************/
  SC_A2R.TCK = card[SC_A2R.Hlength + 2 + SC_A2R.Tlength];
 
   if(SC_A2R.T0) 
   {
       SC_ATR_Size = ((SC_A2R.Tlength)+(SC_A2R.Hlength)+1);  //to set the length of ATR bytes (+1) to inlclude T0, Ts length = H+T+TS+T0-1
  
   }else
   {/*do nothing*/
   }
    return (uint8_t)protocol;
    /********************************* SetATRLength**********/
  
}

void SC_Update(void)

{

     
    static uint16_t SC_counter=0;



    SC_counter+=OS_TICK_MS;
    if (SC_counter!=SC_PERIOD_MS )
    {
       return;
    }
    SC_counter=0;
    R_WDT_Restart();

    if( SC_GetStatus() == SC_REMOVED)
    {
        Deactivation();

    }
    switch(SCState)         // the time between execution of each two consecutive states = SC_PERIOD_MS
     {
        
        case SC_IDLE:
            if(SC_GetStatus() == SC_INSERTED)
            {       
                SCState = SC_ACTIVATION;
            }   
             else
            {
                /* Do Nothing*/
            }
            break;
        case SC_ACTIVATION: 
            
            Activation();
            SCState = SC_WAITRXDATA; // wait for data
            PrevSCState = SC_ACTIVATION;// define prev state as the received data is ATR data after activation 
            break;
        case SC_WAITRXDATA:
            WaitRxData();     


            break;
        case SC_APDU:  // note that state is changed in WriteByte function
                
            WriteAPDUdata();      // write one apdu byte each loop
//        	if(R_UART0_Send(apduCmd,1))
//        	    	{
//        	    		R_UART0_Send(apduCmd,2);
//        	    	}
            break; 
        case SC_DEACTIVATE:
            Deactivation();  
            break;
        default:
         
            break;
    }
}


tSC_Status SC_GetStatus(void)
{
    tSC_Status ret;
    if(SW_GetState(SW_CARD) == SW_PRE_PRESSED  ) 
    {
        ret = SC_INSERTED;      
    }
    else if(SW_GetState(SW_CARD) == SW_PRE_RELEASED )
    {
         ret = SC_REMOVED;
        /* Do Nothing*/
    }
    else
    {
        ret = 0x03;
    }
    return(ret);
}
tSC_State SC_GetState(void)
{
    return (SCState);
}
 void SC_CharErrorSet(uint8_t ErrValue)
{
    CharErrorFlag = ErrValue;
}
 uint8_t SC_CharErrorGet(void)
{
    return CharErrorFlag;
}

/***********************************   write byte according to iec standard try to resent five times untill give up  ********/

 void WriteAPDUdata(void)
 {
	 static uint8_t apduCmd[40];
	 static uint8_t apduCmd2[40];
     static  uint8_t index = 0;   // index = length of apdu data bytes +
     static uint8_t PrevIndex = 0xFF;//ANY KNOWN VALUE
     static uint8_t  RetryCnt = 0;
     // CLA, INS ,P1 , P2 , LC , DATA ,LE


         SC_ADPU_Command.Header.CLA = 0x00;
         SC_ADPU_Command.Header.INS = SC_GET_CHALLENGE;
         SC_ADPU_Command.Header.P_1 = 0x00;
         SC_ADPU_Command.Header.P_2 = 0x00;
         SC_ADPU_Command.Body.LC = 0x04;
         SC_ADPU_Command.Body.LE = 0x00;

     	 apduCmd[0] = 0x00;//0x00;   CLA       // this code request generation of random numbers from sc
     	 apduCmd[1] = 0x84;           //SC_ADPU_Command.Header.INS;   //0x84;INS
     	 apduCmd[2] = 0X00;   //0x00;P1
     	 apduCmd[3] = 0x00;  //0x00;P2
     	 apduCmd[4] = 0X08;   //0x04;LC

     	 apduCmd2[0] = 0x00;//0x00;   CLA       // this code request generation of random numbers from sc
		 apduCmd2[1] = 0x84;           //SC_ADPU_Command.Header.INS;   //0x84;INS
		 apduCmd2[2] = 0X00;   //0x00;P1
		 apduCmd2[3] = 0x00;  //0x00;P2
		 apduCmd2[4] = 0X08;   //0x04;LC


	 U0TXEN ;   // TRANSMIT ENABLE
	 U0RCDIS;    // RECEIVE disable
     if (SC_CharErrorGet() == 0) // if == 0 then no error
         {
             if (PrevIndex != 0XFF)
             {
            	 R_UART0_Send(&apduCmd[PrevIndex],1);// send data and check error after send interrupt

                   PrevIndex = 0xFF;
                 if (RetryCnt == 4)
                 {
                    RetryCnt = 0;
                   SCState = SC_DEACTIVATE;   // DEACTIVATE
                 }
             }
             else
             {
                 RetryCnt = 0;
                 R_UART0_Send(&apduCmd[index],1);
                 index++;
             }
         }
     else
     {
         SC_CharErrorSet(0);   // clear error flag
         PrevIndex = index-1;
         RetryCnt++;
     }
     if (index == 5) // last byte = DATA +1
     {
    	 while (((SSR00 & _0040_SAU_UNDER_EXECUTE) != 0U))//  this condition =0 when uart end transmission
    	 	{};

    	 	 U0TXDIS ;  // Transmit disable
    	 	 U0RCEN ;   // receive enable
         SCState = SC_WAITRXDATA;
         PrevSCState =  SC_APDU;
         index = 0;
     }
 }

 /****************************   read data from uart buffer array **********************************/

 void ReadResponseData(uint8_t * data)
 {
     uint8_t index = 0;
     uint8_t DataResponseSize;
     uint32_t i = 0;

   /* Reset response buffer ---------------------------------------------------*/
     for(i = 0; i < LC_MAX; i++)
     {
         SC_APDU_Response.Data[i] = 0;
     }
     SC_APDU_Response.SW1 = 0;
     SC_APDU_Response.SW2 = 0;

     DataResponseSize =  g_uart0_rx_count;
     for (index = 0;index<(DataResponseSize-2); index++)
     {
         SC_APDU_Response.Data[index] = data[index] ;
     }
     SC_APDU_Response.SW1 = data[index];
     index = index+1;
     SC_APDU_Response.SW2 = data[index];

 }
