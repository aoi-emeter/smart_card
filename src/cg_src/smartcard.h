#ifndef __SMARTCARD_H__
#define __SMARTCARD_H__

#define LC_MAX     20
#include "r_cg_macrodriver.h"
extern  volatile uint16_t  g_uart0_rx_count;            /* uart0 receive data number */

/* Modifiable parameters */
#define SAD           0x0     /* Source address: reader (allowed values 0 -> 7) */
#define DAD           0x0     /* Destination address: card (allowed values 0 -> 7) */
#define IFSD_VALUE    254     /* Max length of INF field Supported by the reader */
#define SC_FILE_SIZE  0x100   /* File size */
#define SC_FILE_ID    0x0001  /* File identifier */
#define SC_CLASS      0x00    

/* Constant parameters */
#define INS_SELECT_FILE    0xA4 /* Select file instruction */
#define INS_READ_FILE      0xB0 /* Read file instruction */
#define INS_WRITE_FILE     0xD6 /* Write file instruction */
#define TRAILER_LENGTH     2    /* Trailer lenght (SW1 and SW2: 2 bytes) */
/* SC Tree Structure -----------------------------------------------------------
                              MasterFile
                           ________|___________
                          |        |           |
                        System   UserData     Note
------------------------------------------------------------------------------*/

/* SC ADPU Command: Operation Code -------------------------------------------*/
#define SC_CLA_GSM11              0xA0

/*------------------------ Data Area Management Commands ---------------------*/
#define SC_SELECT_FILE            0xA4
#define SC_GET_RESPONCE           0xC0
#define SC_STATUS                 0xF2
#define SC_UPDATE_BINARY          0xD6
#define SC_READ_BINARY            0xB0
#define SC_WRITE_BINARY           0xD0
#define SC_UPDATE_RECORD          0xDC
#define SC_READ_RECORD            0xB2

/*-------------------------- Administrative Commands -------------------------*/ 
#define SC_CREATE_FILE            0xE0

/*-------------------------- Safety Management Commands ----------------------*/
#define SC_VERIFY                 0x20
#define SC_CHANGE                 0x24
#define SC_DISABLE                0x26
#define SC_ENABLE                 0x28
#define SC_UNBLOCK                0x2C
#define SC_EXTERNAL_AUTH          0x82
#define SC_GET_CHALLENGE          0x84

/*-------------------------- Smartcard Interface Byte-------------------------*/
#define SC_INTERFACEBYTE_TA    0 /* Interface byte TA(i) */
#define SC_INTERFACEBYTE_TB    1 /* Interface byte TB(i) */
#define SC_INTERFACEBYTE_TC    2 /* Interface byte TC(i) */
#define SC_INTERFACEBYTE_TD    3 /* Interface byte TD(i) */

/*-------------------------- Answer to reset Commands ------------------------*/ 
#define SC_GET_A2R                0x00

/* SC STATUS: Status Code ----------------------------------------------------*/
#define SC_EF_SELECTED            0x9F
#define SC_DF_SELECTED            0x9F
#define SC_OP_TERMINATED         0x9000



 /* Smartcard Voltage */
#define SC_VOLTAGE_5V              0
#define SC_VOLTAGE_3V              1

typedef enum    
{
    SC_INSERTED = 0,
    SC_REMOVED
}tSC_Status;
/**********************************sc states****************************/
typedef enum
{
  SC_IDLE = 0,            
  SC_ACTIVATION ,
  SC_WAITRXDATA,
  SC_APDU,
  SC_DEACTIVATE
} tSC_State;
/********************************counter names*******************************/
typedef enum    
{
    SC_POINT_TWO_ETU_Counter = 0,
    SC_ETU_Counter, 
    SC_GT_Counter ,
    SC_CLK_RESTART_Counter,
    SC_HALFETU_Counter ,
    SC_ERR_Counter ,
    SC_CLK_STOP_Counter ,
    SC_WT_Counter ,
    SC_RESET_Counter      //RESET width signal
 
}tSC_CounterType;

/***********************************SC communicatin state (TX / RX)**********/
typedef enum    
{
 SC_Transmiting = 0,
 SC_Receiving ,
 SC_RESETTING               //RESETTING the sc
}tSC_CommState;


/***************APDU SECTION*********************/
/* ADPU-Header command structure ---------------------------------------------*/
//typedef struct
//{
//  uint8_t CLA;  /* Command class */
//  uint8_t INS;  /* Operation code */
// // uint8_t P1;   /* Selection Mode */
////  uint8_t P2;   /* Selection Option */
//} tSC_Header;
typedef struct
{
  uint8_t CLA;  /* Command class */
  uint8_t INS;  /* Operation code */
  uint8_t P_1;   /* Selection Mode */
  uint8_t P_2;   /* Selection Option */
} tSC_Header;

/* ADPU-Body command structure -----------------------------------------------*/
typedef struct
{
  uint8_t LC;           /* Data field length */
  uint8_t Data[LC_MAX]; /* Command parameters */
  uint8_t LE;           /* Expected length of data to be returned */
} tSC_Body;

/* ADPU Command structure ----------------------------------------------------*/
typedef struct
{
  tSC_Header Header;
  tSC_Body Body;
} tSC_ADPU_Commands;

/* SC response structure -----------------------------------------------------*/
typedef struct
{
  uint8_t Data[LC_MAX];  /* Data returned from the card */
  uint8_t SW1;          /* Command Processing status */
  uint8_t SW2;          /* Command Processing qualification */
} tSC_ADPU_Response;

/***********************     SC functions prototypes **********************/
void SC_Init(void);// initialize timers , clock , uart 
void SC_Update(void);// change sc state according to operation , update variables , will be called by periodic int

tSC_Status SC_GetStatus(void);  //return the status of sc if inserted or removed
tSC_State SC_GetState(void);    //return the state of sc if (ideal , wait ,Data exchange apdu , activation , deactivation)
void WriteAPDUdata(void); // write apdu command
//void ReadResponseData(uint8_t * data);// receive the data and response from sc

 void SC_CharErrorSet(uint8_t ErrValue);
 uint8_t SC_CharErrorGet(void);
 /************************** macros *******************/

#endif /* __SMARTCARD_H__ */
