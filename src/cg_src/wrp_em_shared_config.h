/******************************************************************************
  Copyright (C) 2011 Renesas Electronics Corporation, All Rights Reserved.
*******************************************************************************
* File Name    : wrp_em_shared_config.h
* Version      : 1.00
* Description  : Wrapper EM Shared Configuration Header file
******************************************************************************
* History : DD.MM.YYYY Version Description
******************************************************************************/

#ifndef _WRAPPER_EM_SHARED_CONFIG_H
#define _WRAPPER_EM_SHARED_CONFIG_H

/******************************************************************************
Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_cg_macrodriver.h"

/******************************************************************************
*   EM Core Driver Shared Block
*******************************************************************************/
    /**************************************************************************
    *   MCU Driver Header
    ***************************************************************************/
    
    /**************************************************************************
    *   WDT Driver Header
    ***************************************************************************/
    #include "r_cg_wdt.h"
    
    /**************************************************************************
    *   EM TIMER Driver Header
    ***************************************************************************/
    #include "r_cg_tau.h"
    
    /**************************************************************************
    *   RTC Driver Header
    ***************************************************************************/
//    #include "r_cg_rtc.h"
    
    
/******************************************************************************
Macro definitions
******************************************************************************/

/******************************************************************************
*   EM Core Shared Block
*******************************************************************************/
    /**************************************************************************
    *   MCU Wrapper Configuration for Settings & I/F mapping
    ***************************************************************************/
    /* I/Fs mapping
     *      Macro Name / I/Fs                                   Setting/Description */
    #define EM_MCU_MULTIPLE_INT_ENABLE_STATEMENT                {EI();}                         /* Multiple interrupt enable statement */
    #define EM_MCU_MULTIPLE_INT_DISABLE_STATEMENT               {DI();}                         /* Multiple interrupt disable statement */

    /**************************************************************************
    *   WDT Wrapper Configuration for Settings & I/F mapping
    ***************************************************************************/
    /* I/Fs mapping
     *      Macro Name / I/Fs                                   Setting/Description */
    #define EM_WDT_DriverInit()                                 R_WDT_Create()                  /* WDT Init */
    #define EM_WDT_DriverStart()                                {;}                             /* WDT Start */
    #define EM_WDT_DriverStop()                                 {;}                             /* WDT Stop */
    #define EM_WDT_DriverRestart()                              R_WDT_Restart()                 /* WDT Restart */

    /**************************************************************************
    *   EM TIMER Wrapper Configuration for Settings & I/F mapping
    ***************************************************************************/
    /* I/Fs mapping
     *      Macro Name / I/Fs                                   Setting/Description */
    #define EM_TIMER_DriverInit()                               {;}                             /* Init */
    #define EM_TIMER_DriverStart()                              R_TAU0_Channel2_Start()         /* Start */
    #define EM_TIMER_DriverStop()                               R_TAU0_Channel2_Stop()          /* Stop */

    /**************************************************************************
    *   RTC Wrapper Configuration for Settings & I/F mapping
    ***************************************************************************/
    /* I/Fs mapping
//     *      Macro Name / I/Fs                                   Setting/Description */
//    #define EM_RTC_DriverInit()                                 {;}                             /* Init */
//    #define EM_RTC_DriverStart()                                R_RTC_Start()                   /* Start */
//    #define EM_RTC_DriverStop()                                 {;}                             /* Stop */
//    #define EM_RTC_DriverCounterGet(datetime)                   R_RTC_Get_CalendarCounterValue(datetime)                            /* RTC Get Counter Value BCD Format */

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Variable Externs
******************************************************************************/

/******************************************************************************
Functions Prototypes
******************************************************************************/

#endif /* _WRAPPER_CORE_CONFIG_H */
