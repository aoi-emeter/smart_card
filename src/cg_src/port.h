#ifndef __PORT_H__
#define __PORT_H__

//#include "main.h"

/* Fill missing code below */
/* Switches */

#define SW_CARD_PORT_DR     (GPIO_PORTE_DATA)
#define SW_CARD_PORT_CR     (GPIO_PORTE_CONTROL)
#define SW_CARD_PIN         (GPIO_PIN_7)



/* Signals */
#define SC_RESET_PORT_DR     (GPIO_PORTE_DATA)
#define SC_RESET_PORT_CR     (GPIO_PORTE_CONTROL)
#define SC_RESET_PIN         (P5_bit.no6)

#define SC_CLK_PORT_DR     (GPIO_PORTE_DATA)
#define SC_CLK_PORT_CR     (GPIO_PORTE_CONTROL)
#define SC_CLK_PIN         (GPIO_PIN_4)

#define UART_TX_PIN_DR     (GPIO_PORTG_DATA)
#define UART_TX_PIN_CR     (GPIO_PORTG_CONTROL)
#define UART_TX_PIN        (GPIO_PIN_1)

#define UART_RX_PIN_DR     (GPIO_PORTG_DATA)
#define UART_RX_PIN_CR     (GPIO_PORTG_CONTROL)
#define UART_RX_PIN        (GPIO_PIN_2)



#define LOW                (0)
#define HIGH               (1)
#define DISABLE            (0)
#define ENABLE             (1)


#define DIGITAL_BITS0_ON         (ANCON0=0x00)
#define DIGITAL_BITS1_ON         (ANCON1=0x00)
#define DIGITAL_BITS2_ON         (ANCON2=0x00)
#endif // __PORT_H__
