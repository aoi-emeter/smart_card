#ifndef __SW_H__
#define __SW_H__


typedef enum
{
    SW_CARD,
}tSW;

typedef enum
{
    SW_RELEASED,
    SW_PRE_PRESSED,
    SW_PRESSED,
    SW_PRE_RELEASED

}tSW_State;

/*Functions prototype*/
void SW_Init(tSW sw, tSW_State state);
void SW_Update(void);
tSW_State SW_GetState(tSW sw);
//uint16_t SW_GetPressTime(tSW sw);


#endif // __SW_H__
