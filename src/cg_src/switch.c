#include "main.h"
#include "switch.h"
#include "port.h"
#include "r_cg_tau.h"
//#include "OS.h"
//#include  <xc.h>
#include "r_cg_macrodriver.h"

#define SW_PERIOD_MS   (20)
#define N_SAMPLES   (2)
#define N_SWITCHES  (1)
#define SW_RELEASED_LEVEL       (1)//1
#define SW_PRESSED_LEVEL        (0)//0
#define SW_MAX_PRESS_TIME_MS    ((uint16_t) (~0) )

typedef struct
{
    tSW_State state;
    uint8_t samples[N_SAMPLES];
    uint16_t press_time;
}tSW_Info;

static tSW_Info switches_info[N_SWITCHES];


void SW_Init(tSW sw, tSW_State state)
{
    
    /* Initialize sw pin as input */
    switch (sw)
    {
    case SW_CARD:
        //GPIO_InitPortPin(SW_CARD_PORT_CR, SW_CARD_PIN, GPIO_IN);

        break;

    default:
        /* should not be here */
        break;
    }

    /* Initialize switch info: samples and state */
    switches_info[sw].state = state;
    switches_info[sw].press_time = 0;

    switch (state)
    {
    case SW_RELEASED:
    case SW_PRE_RELEASED:
        switches_info[sw].samples[0] = SW_RELEASED_LEVEL;
        switches_info[sw].samples[1] = SW_RELEASED_LEVEL;
        break;
    case SW_PRE_PRESSED:
    case SW_PRESSED:
        switches_info[sw].samples[0] = SW_PRESSED_LEVEL;
        switches_info[sw].samples[1] = SW_PRESSED_LEVEL;
        break;
    default:
        /* should not be here */
        break;

    }


}

void SW_Update(void)
{
   static uint8_t SW_counter=0;
   uint8_t switch_index;

    SW_counter+=OS_TICK_MS;
    if (SW_counter!=SW_PERIOD_MS)
    {
       return;
    }
    SW_counter=0;
//PORTEbits.RE1 = ~PORTEbits.RE1; // for testing pulse width


    for(switch_index = SW_CARD; switch_index < N_SWITCHES; switch_index++)
    {
        /* Update samples */
        switches_info[switch_index].samples[0] = switches_info[switch_index].samples[1];
        if(switch_index == SW_CARD)
        {
            switches_info[switch_index].samples[1] =P5_bit.no5;
        } 
        else{
            /*should not be here*/
        }

        /* Update state */
        switch (switches_info[switch_index].state)
        {
        case SW_RELEASED:
            if ((switches_info[switch_index].samples[0] == SW_PRESSED_LEVEL)&&
                (switches_info[switch_index].samples[1] == SW_PRESSED_LEVEL))
            {
                switches_info[switch_index].state=SW_PRE_PRESSED;
            }

            break;
        case SW_PRE_PRESSED:
            if ((switches_info[switch_index].samples[1] == SW_PRESSED_LEVEL))
            {
                switches_info[switch_index].state = SW_PRESSED;
                switches_info[switch_index].press_time = 0;
            }
            break;

        case SW_PRESSED:
            if ((switches_info[switch_index].samples[0] == SW_RELEASED_LEVEL)&&
                (switches_info[switch_index].samples[1] == SW_RELEASED_LEVEL))
            {
                switches_info[switch_index].state = SW_PRE_RELEASED;
                switches_info[switch_index].press_time = 0;
            }else if(switches_info[switch_index].press_time < SW_MAX_PRESS_TIME_MS)
            {
                switches_info[switch_index].press_time += SW_PERIOD_MS;
            }else
            {
               /*You should not be here*/
            }
            break;

        case SW_PRE_RELEASED:
            if ((switches_info[switch_index].samples[1] == SW_RELEASED_LEVEL))
            {
                switches_info[switch_index].state = SW_RELEASED;
            }
            break;

        default:

            break;

        }

    }

}

tSW_State SW_GetState(tSW sw)
{
    return(switches_info[sw].state);
}



/*uint16_t SW_GetPressTime(tSW sw)
{
    return(switches_info[sw].press_time);
}*/
