################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables
C_SRCS += \
../src/cg_src/r_cg_cgc.c \
../src/cg_src/r_cg_cgc_user.c \
../src/cg_src/r_cg_main.c \
../src/cg_src/r_cg_port.c \
../src/cg_src/r_cg_port_user.c \
../src/cg_src/r_cg_sau.c \
../src/cg_src/r_cg_sau_user.c \
../src/cg_src/r_cg_systeminit.c \
../src/cg_src/r_cg_tau.c \
../src/cg_src/r_cg_tau_user.c \
../src/cg_src/r_cg_wdt.c \
../src/cg_src/r_cg_wdt_user.c \
../src/cg_src/smartcard.c \
../src/cg_src/switch.c \
../src/cg_src/wrp_mcu.c 

COMPILER_OBJS += \
src/cg_src/r_cg_cgc.obj \
src/cg_src/r_cg_cgc_user.obj \
src/cg_src/r_cg_main.obj \
src/cg_src/r_cg_port.obj \
src/cg_src/r_cg_port_user.obj \
src/cg_src/r_cg_sau.obj \
src/cg_src/r_cg_sau_user.obj \
src/cg_src/r_cg_systeminit.obj \
src/cg_src/r_cg_tau.obj \
src/cg_src/r_cg_tau_user.obj \
src/cg_src/r_cg_wdt.obj \
src/cg_src/r_cg_wdt_user.obj \
src/cg_src/smartcard.obj \
src/cg_src/switch.obj \
src/cg_src/wrp_mcu.obj 

C_DEPS += \
src/cg_src/r_cg_cgc.d \
src/cg_src/r_cg_cgc_user.d \
src/cg_src/r_cg_main.d \
src/cg_src/r_cg_port.d \
src/cg_src/r_cg_port_user.d \
src/cg_src/r_cg_sau.d \
src/cg_src/r_cg_sau_user.d \
src/cg_src/r_cg_systeminit.d \
src/cg_src/r_cg_tau.d \
src/cg_src/r_cg_tau_user.d \
src/cg_src/r_cg_wdt.d \
src/cg_src/r_cg_wdt_user.d \
src/cg_src/smartcard.d \
src/cg_src/switch.d \
src/cg_src/wrp_mcu.d 

# Each subdirectory must supply rules for building sources it contributes
src/cg_src/%.obj: ../src/cg_src/%.c src/cg_src/Compiler.sub
	@echo 'Scanning and building file: $<'
	@echo 'Invoking: Scanner and Compiler'
	@echo src\cg_src\cDepSubCommand.tmp=
	@sed -e "s/^/ /" "src\cg_src\cDepSubCommand.tmp"
	ccrl -subcommand="src\cg_src\cDepSubCommand.tmp" -o "$(@:%.obj=%.d)"  -MT="$(@:%.obj=%.obj)"  -MT="$(@:%.obj=%.d)" -msg_lang=english "$<"
	@echo src\cg_src\cSubCommand.tmp=
	@sed -e "s/^/ /" "src\cg_src\cSubCommand.tmp"
	ccrl -subcommand="src\cg_src\cSubCommand.tmp" -msg_lang=english -o "$(@:%.d=%.obj)" "$<"
	@echo 'Finished Scanning and building: $<'
	@echo.

